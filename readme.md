# Ejemplo: Load Balancing with IaC

Este ejemplo busca demostrar una arquitectura sencilla de balanceo de cargas implementando IaC.

# Cómo funciona

Vaya a la carpeta raíz del clon de este repositorio y ejecute el proyecto:
	```
	docker-compose up
	```